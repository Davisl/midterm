package com.example.midterm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.AudioManager;

import android.media.SoundPool;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity {

    SoundPool pl = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
    private Button SecondPageButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle myextras = getIntent().getExtras();


        ImageView pandf = (ImageView) findViewById(R.id.paf);
        ImageView dbz = (ImageView) findViewById(R.id.dbz);
        ImageView yugi = (ImageView) findViewById(R.id.yugi);
        ImageView zim = (ImageView) findViewById(R.id.zim);

        configureNextButton();



        pl.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                soundPool.play(sampleId, 1f, 1f, 0, 0, 1);
                }
        });


        pandf.setOnClickListener(new View.OnClickListener() {
             @Override
            public void onClick(View v) {

                int sound = pl.load(MainActivity.this, R.raw.cake, 0);

            }
        });
        dbz.setOnClickListener(new View.OnClickListener() {
             @Override
            public void onClick(View v) {

                int sound = pl.load(MainActivity.this, R.raw.kame, 0);

            }
        });
        zim.setOnClickListener(new View.OnClickListener() {
     @Override
            public void onClick(View v) {

                int sound = pl.load(MainActivity.this, R.raw.lies, 0);

            }
        });
        yugi.setOnClickListener(new View.OnClickListener() {
         @Override
            public void onClick(View v) {

                int sound = pl.load(MainActivity.this, R.raw.duel, 0);

            }
        });


    }
    private void configureNextButton(){
        Button nextButton = (Button) findViewById(R.id.btnNext);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, page2.class));
            }
        });
    }
}
